import sys, os
import pandas as pd

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

import django
django.setup()

from boletin.models import Registrados

def save_wine_from_row(wine_row):
    wine = Registrados()
    print(wine_row[0], type(wine_row[0]))
    wine.nombre = str(wine_row[0])
    wine.x_val = wine_row[1]
    wine.y_val = wine_row[2]
    wine.save()


if __name__ == "__main__":

    if len(sys.argv) == 2:
        print
        "Reading from file " + str(sys.argv[1])
        wines_df = pd.read_csv(sys.argv[1])
        print
        wines_df

        wines_df.apply(
            save_wine_from_row,
            axis=1
        )

        print
        "There are {} wines".format(Registrados.objects.count())

    else:
        print
        "Please, provide Wine file path"