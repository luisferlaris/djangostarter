from django.contrib import admin

# Register your models here.

from .models import Registrados

class AdminRegistrado(admin.ModelAdmin):
    list_display = ["__str__", "x_val","y_val", "timestamp"]
    list_filter = ["timestamp"]
    search_fields = ["nombre"]
    class Meta:
        model = Registrados

admin.site.register(Registrados, AdminRegistrado)