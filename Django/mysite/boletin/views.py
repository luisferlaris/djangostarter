from django.shortcuts import render_to_response
from boletin.models import Registrados

from django.template import RequestContext

from django.db import connections
from django.db.models import Count
from django.http import JsonResponse
from django.shortcuts import render

#from .models import Play


def graph(request):
    return render(request, 'graph.html')


def play_count_by_month(request):
    data = Registrados.objects.all().values()
    return JsonResponse(list(data), safe=False)

# Create your views here.

def inicio(request):
    context = RequestContext(request)
    category_list = Registrados.objects.all()
    context_dict = {'categories': category_list}
    regs = Registrados.objects.values()
    #data = [{'id': reg.nombre, 'x_val': reg.x_val} for reg in regs]
    return(render_to_response("inicio.html" ,context_dict,context ))