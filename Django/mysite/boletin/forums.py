from boletin.models import Registrados
from django import forms

class RegForm(forms.ModelForm):
    class Meta:
        Model = Registrados
        fields = ["x_val","y_val"]

