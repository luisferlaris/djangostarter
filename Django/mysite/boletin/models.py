from django.db import models

# Create your models here.

class Registrados(models.Model):
    nombre = models.CharField(max_length= 100, blank = True, null = True)
    x_val = models.DecimalField( max_digits=5, decimal_places=2)
    y_val = models.DecimalField( max_digits=5, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add= True, auto_now= False)

    def __str__(self):
        return self.nombre
